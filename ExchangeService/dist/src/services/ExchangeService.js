"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExchangeService = void 0;
const ExchangeRepository_1 = require("../repositories/ExchangeRepository");
class ExchangeService {
    constructor() {
        this.exchangeRepository = new ExchangeRepository_1.ExchangeRepository();
    }
    createExchangeRequest(details) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.exchangeRepository.createExchange(details);
        });
    }
    acceptExchangeRequest(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const updated = yield this.exchangeRepository.updateExchangeStatus(id, 'accepted');
            if (updated[1].length != 0) {
                return updated[1][0];
            }
            else {
                return null;
            }
        });
    }
    rejectExchangeRequest(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const updated = yield this.exchangeRepository.updateExchangeStatus(id, 'rejeceted');
            if (updated[1].length != 0) {
                return updated[1][0];
            }
            else {
                return null;
            }
        });
    }
    getExchangesForUser(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.exchangeRepository.listExchangesForUser(userId);
        });
    }
}
exports.ExchangeService = ExchangeService;
