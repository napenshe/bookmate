"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Exchange = void 0;
const sequelize_1 = require("sequelize");
const config_1 = __importDefault(require("../config/config"));
class Exchange extends sequelize_1.Model {
}
exports.Exchange = Exchange;
Exchange.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    requesterId: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false
    },
    responderId: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false
    },
    requesterBookId: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false
    },
    responderBookId: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false
    },
    status: {
        type: sequelize_1.DataTypes.STRING,
        defaultValue: 'requested'
    }
}, {
    sequelize: config_1.default,
    modelName: 'Exchange'
});
