"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExchangeController = void 0;
const ExchangeService_1 = require("../services/ExchangeService");
class ExchangeController {
    constructor() {
        this.createExchange = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const exchange = yield this.exchangeService.createExchangeRequest(req.body);
                res.status(201).json(exchange);
            }
            catch (error) {
                res.status(500).json({ error: error });
            }
        });
        this.acceptExchange = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const exchange = yield this.exchangeService.acceptExchangeRequest(parseInt(req.params.id));
                if (exchange) {
                    res.status(200).json(exchange);
                }
                else {
                    res.status(404).json({ error: 'Exchange request not found' });
                }
            }
            catch (error) {
                res.status(500).json({ error: error });
            }
        });
        this.rejectExchange = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const exchange = yield this.exchangeService.rejectExchangeRequest(parseInt(req.params.id));
                if (exchange) {
                    res.status(200).json(exchange);
                }
                else {
                    res.status(404).json({ error: 'Exchange request not found' });
                }
            }
            catch (error) {
                res.status(500).json({ error: error });
            }
        });
        this.listExchanges = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const exchanges = yield this.exchangeService.getExchangesForUser(parseInt(req.user.id));
                res.status(200).json(exchanges);
            }
            catch (error) {
                res.status(500).json({ error: error });
            }
        });
        this.exchangeService = new ExchangeService_1.ExchangeService();
    }
}
exports.ExchangeController = ExchangeController;
