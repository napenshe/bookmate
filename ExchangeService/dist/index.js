"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ExchangeController_1 = require("./src/controllers/ExchangeController");
const AuthMiddleware_1 = require("./src/middleware/AuthMiddleware");
const config_1 = __importDefault(require("./src/config/config"));
const app = (0, express_1.default)();
const exchangeController = new ExchangeController_1.ExchangeController();
app.use(express_1.default.json());
app.post('/exchanges', AuthMiddleware_1.authMiddleware, exchangeController.createExchange);
app.put('/exchanges/:id/accept', AuthMiddleware_1.authMiddleware, exchangeController.acceptExchange);
app.put('/exchanges/:id/reject', AuthMiddleware_1.authMiddleware, exchangeController.rejectExchange);
app.get('/exchanges', AuthMiddleware_1.authMiddleware, exchangeController.listExchanges);
config_1.default.sync().then(() => {
    app.listen(process.env.PORT, () => {
        console.log('ExchangeService is running');
    });
}).catch((err) => {
    console.error('Unable to connect to the database:', err);
});
