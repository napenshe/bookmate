import { Router } from 'express';
import { ExchangeController } from '../controllers/ExchangeController';
import { authMiddleware } from '../middleware/AuthMiddleware';

const router = Router();
const exchangeController = new ExchangeController();

router.post('/exchanges', authMiddleware, exchangeController.createExchange);
router.put('/exchanges/:id/accept', authMiddleware, exchangeController.acceptExchange);
router.put('/exchanges/:id/reject', authMiddleware, exchangeController.rejectExchange);
router.put('/exchanges/:id/close', authMiddleware, exchangeController.closeExchange)
router.get('/exchanges', authMiddleware, exchangeController.listExchanges);

export default router;