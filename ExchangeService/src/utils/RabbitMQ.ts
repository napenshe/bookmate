import amqp from 'amqplib';
import 'dotenv/config';

const rabbitUrl = `amqp://${process.env.RABBITMQ_DEFAULT_USER}:${process.env.RABBITMQ_DEFAULT_PASS}@rabbitmq:5672`;

export const connectRabbitMQ = async () => {
    try {
        const connection = await amqp.connect(rabbitUrl);
        const channel = await connection.createChannel();
        await channel.assertQueue('bookAvailability');
        return channel;
    } catch (error) {
        console.error('Error connecting to RabbitMQ', error);
        throw error;
    }
};