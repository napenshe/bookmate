import { ExchangeRepository } from '../repositories/ExchangeRepository';
import { Exchange } from '../models/Exchange';
import { connectRabbitMQ } from '../utils/RabbitMQ';

export class ExchangeService {
    private exchangeRepository: ExchangeRepository;

    constructor() {
        this.exchangeRepository = new ExchangeRepository();
    }

    public async createExchangeRequest(details: any): Promise<Exchange> {
        return this.exchangeRepository.createExchange(details);
    }

    public async acceptExchangeRequest(id: number): Promise<Exchange | null> {
        const exchange = await this.exchangeRepository.findExchangeById(id);
        if (exchange && exchange.status === 'requested') {
            const updateResult = await this.exchangeRepository.updateExchangeStatus(id, 'accepted');
            if (updateResult[0] > 0) {
                const channel = await connectRabbitMQ();
                channel.sendToQueue('bookAvailability', Buffer.from(JSON.stringify({
                    bookId: exchange.requesterBookId,
                    available: false
                })));
                channel.sendToQueue('bookAvailability', Buffer.from(JSON.stringify({
                    bookId: exchange.responderBookId,
                    available: false
                })));
                return updateResult[1][0];
            }
        }
        return null;
    }

    public async rejectExchangeRequest(id: number): Promise<Exchange | null> {
        const exchange = await this.exchangeRepository.findExchangeById(id);
        if (exchange && exchange.status === 'requested') {
            const updated = await this.exchangeRepository.updateExchangeStatus(id, 'rejeceted')
            if (updated[0] > 0) {
                return updated[1][0];
            }
        }
        return null;
    }

    public async closeExchangeRequest(id: number): Promise<Exchange | null> {
        const exchange = await this.exchangeRepository.findExchangeById(id);
        if (exchange && (exchange.status === 'accepted' || exchange.status === 'rejected')) {
            const updated = await this.exchangeRepository.updateExchangeStatus(id, 'closed')
            if (updated[0] > 0) {
                const channel = await connectRabbitMQ();
                channel.sendToQueue('bookAvailability', Buffer.from(JSON.stringify({
                    bookId: exchange.requesterBookId,
                    available: true
                })));
                channel.sendToQueue('bookAvailability', Buffer.from(JSON.stringify({
                    bookId: exchange.responderBookId,
                    available: true
                })));
                return updated[1][0];
            }
        }
        return null;
    }

    public async getExchangesForUser(userId: number): Promise<Exchange[]> {
        return this.exchangeRepository.listExchangesForUser(userId);
    }
}