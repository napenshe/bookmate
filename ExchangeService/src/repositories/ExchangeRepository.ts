import { Op } from 'sequelize';
import { Exchange } from '../models/Exchange';

export class ExchangeRepository {
    public async createExchange(exchangeDetails: any): Promise<Exchange> {
        return Exchange.create(exchangeDetails);
    }

    public async findExchangeById(id: number): Promise<Exchange | null> {
        return Exchange.findByPk(id);
    }

    public async updateExchangeStatus(id: number, status: string): Promise<[number, Exchange[]]> {
        return Exchange.update({ status }, { where: { id }, returning: true });
    }

    public async listExchangesForUser(userId: number): Promise<Exchange[]> {
        return Exchange.findAll({
            where: {
                [Op.or]: [
                    { requesterId: userId },
                    { responderId: userId }
                ]
            }
        });
    }
}