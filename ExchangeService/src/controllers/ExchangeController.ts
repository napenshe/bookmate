import { Request, Response } from 'express';
import { ExchangeService } from '../services/ExchangeService';

export class ExchangeController {
    private exchangeService: ExchangeService;

    constructor() {
        this.exchangeService = new ExchangeService();
    }

    public createExchange = async (req: Request, res: Response): Promise<void> => {
        try {
            const exchange = await this.exchangeService.createExchangeRequest(req.body);
            res.status(201).json(exchange);
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };

    public acceptExchange = async (req: Request, res: Response): Promise<void> => {
        try {
            const exchange = await this.exchangeService.acceptExchangeRequest(parseInt(req.params.id));
            if (exchange) {
                res.status(200).json(exchange);
            } else {
                res.status(404).json({ error: 'Exchange request not found' });
            }
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };

    public rejectExchange = async (req: Request, res: Response): Promise<void> => {
        try {
            const exchange = await this.exchangeService.rejectExchangeRequest(parseInt(req.params.id));
            if (exchange) {
                res.status(200).json(exchange);
            } else {
                res.status(404).json({ error: 'Exchange request not found' });
            }
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };

    public closeExchange = async (req: Request, res: Response): Promise<void> => {
        try {
            const exchange = await this.exchangeService.closeExchangeRequest(parseInt(req.params.id));
            if (exchange) {
                res.status(200).json(exchange);
            } else {
                res.status(404).json({ error: 'Exchange request not found' });
            }
        } catch (error) {
            res.status(500).json({ error: error });
        }
    }

    public listExchanges = async (req: Request, res: Response): Promise<void> => {
        try {
            const exchanges = await this.exchangeService.getExchangesForUser(parseInt(req.user.id));
            res.status(200).json(exchanges);
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };
}