import { DataTypes, Model } from 'sequelize';
import sequelize from '../config/config';

export class Exchange extends Model {
    public id!: number;
    public requesterId!: number;
    public responderId!: number;
    public requesterBookId!: number;
    public responderBookId!: number;
    public status!: string; // ('requested', 'accepted', 'rejected', 'closed')
}

Exchange.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  requesterId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  responderId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  requesterBookId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  responderBookId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  status: {
    type: DataTypes.STRING,
    defaultValue: 'requested'
  }
}, {
  sequelize,
  modelName: 'Exchange'
});