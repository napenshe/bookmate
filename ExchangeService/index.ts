import express from 'express';
import sequelize from './src/config/config';
import exchangeRoutes from './src/routes/ExchangeRoutes';

const app = express();

app.use(express.json());
app.use(exchangeRoutes);

sequelize.sync().then(() => {
  app.listen(process.env.PORT, () => {
    console.log('ExchangeService is running');
  });
}).catch((err) => {
    console.error('Unable to connect to the database:', err);
});