"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const User_1 = __importDefault(require("../models/User"));
require("dotenv/config");
class AuthController {
    register(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // try {
            //   const { username, password } = req.body;
            //   const hashedPassword = await bcrypt.hash(password, 10);
            //   const user = await User.create({
            //     username,
            //     password: hashedPassword
            //   });
            //   res.status(201).send({ message: 'User registered successfully', user });
            // } catch (error) {
            //   res.status(500).send({ message: 'Error registering user', error });
            // }
            res.status(201).send(process.env.JWT_SECRET);
        });
    }
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { username, password } = req.body;
                const user = yield User_1.default.findOne({ where: { username } });
                if (!user) {
                    return res.status(404).send({ message: 'User not found' });
                }
                // const isMatch = await bcrypt.compare(password, user.password);
                // if (!isMatch) {
                //   return res.status(401).send({ message: 'Invalid credentials' });
                // }
                // const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, { expiresIn: '1h' });
                res.status(200).send({ message: 'Logged in successfully' });
            }
            catch (error) {
                res.status(500).send({ message: 'Error logging in', error });
            }
        });
    }
}
exports.AuthController = AuthController;
;
