import { UserRepository } from '../repositories/UserRepository';
import { User } from '../models/User';
import jwt from 'jsonwebtoken';
import { connectRabbitMQ } from '../utils/RabbitMQ';

export class AuthService {
    private userRepository: UserRepository;

    constructor() {
        this.userRepository = new UserRepository();
    }

    public async register(userDetails: any): Promise<User> {
        const existingUser = await this.userRepository.findUserByEmail(userDetails.email);
        if (existingUser) {
            throw new Error('Email already in use');
        }
        const user = await this.userRepository.createUser(userDetails);
        const channel = await connectRabbitMQ();
        channel.sendToQueue('userRegistry', Buffer.from(JSON.stringify({
            userId: user.id
        })));
        return user
    }

    public async login(email: string, password: string): Promise<string> {
        const user = await this.userRepository.findUserByEmail(email);
        if (!user) {
            throw new Error('User not found');
        }
        const isMatch = await this.userRepository.comparePassword(password, user.password);
        if (!isMatch) {
            throw new Error('Invalid credentials');
        }

        return jwt.sign({ id: user.id, email: user.email }, process.env.JWT_SECRET!, { expiresIn: '1h' });
    }
}