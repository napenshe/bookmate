import swaggerJsdoc from 'swagger-jsdoc';
const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Auth Service of BookMate',
      version: '1.0.0',
      description: 'This is a simple API for Authentication Service',
    },
    servers: [
      {
        url: 'http://158.160.171.86:8001',
      },
    ],
  },
  apis: ['./src/routes/*.js']
};

export const swaggerSpec = swaggerJsdoc(options);