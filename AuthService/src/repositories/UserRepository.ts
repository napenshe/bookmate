import { User } from '../models/User';
import bcrypt from 'bcryptjs';

export class UserRepository {
    private saltRounds: number = 10;

    public async createUser(userDetails: any): Promise<User> {
        const hashedPassword = await bcrypt.hash(userDetails.password, this.saltRounds);
        userDetails.password = hashedPassword;
        return User.create(userDetails);
    }

    public async findUserByEmail(email: string): Promise<User | null> {
        return User.findOne({ where: { email } });
    }

    public async comparePassword(candidatePassword: string, storedPassword: string): Promise<boolean> {
        return bcrypt.compare(candidatePassword, storedPassword);
    }
}