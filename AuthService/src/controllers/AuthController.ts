import { Request, Response } from 'express';
import { AuthService } from '../services/AuthService';

export class AuthController {
    private authService: AuthService;

    constructor() {
        this.authService = new AuthService();
    }

    public register = async (req: Request, res: Response): Promise<void> => {
        try {
            const user = await this.authService.register(req.body);
            res.status(201).json(user);
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };

    public login = async (req: Request, res: Response): Promise<void> => {
        try {
            const token = await this.authService.login(req.body.email, req.body.password);
            res.status(200).json({ token });
        } catch (error) {
            res.status(401).json({ error: error });
        }
    };
}