import express from 'express';
import swaggerUi from 'swagger-ui-express';
import 'dotenv/config';

import authRoutes from './src/routes/AuthRoutes';
import sequelize from './src/config/config';
import { swaggerSpec } from './src/swagger/swaggerConfig';

const app = express();

app.use(express.json());
app.use(authRoutes);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

sequelize.sync().then(() => {
  app.listen(process.env.PORT, () => {
    console.log('AuthService is running');
  });
}).catch((err) => {
  console.error('Unable to connect to the database:', err);
});