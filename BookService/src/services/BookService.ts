import { BookRepository } from '../repositories/BookRepository';
import { Book } from '../models/Book';

export class BookService {
    private bookRepository: BookRepository;

    constructor() {
        this.bookRepository = new BookRepository();
    }

    public async addBook(bookDetails: any): Promise<Book> {
        return this.bookRepository.createBook(bookDetails);
    }

    public async listBooks(userId: number): Promise<Book[]> {
        return this.bookRepository.getBooksByUserId(userId);
    }
}