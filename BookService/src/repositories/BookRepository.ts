import { Book } from '../models/Book';

export class BookRepository {
    public async createBook(bookDetails: any): Promise<Book> {
        return Book.create(bookDetails);
    }

    public async getBooksByUserId(userId: number): Promise<Book[]> {
        return Book.findAll({ where: { userId } });
    }

    public async updateBookAvailability(id: number, available: boolean): Promise<[number, Book[]]> {
        return Book.update({ available }, { where: { id }, returning: true });
    }
}