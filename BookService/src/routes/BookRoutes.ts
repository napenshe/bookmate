import { Router } from 'express';
import { BookController } from '../controllers/BookController';
import { authMiddleware } from '../middleware/AuthMiddleware';

const router = Router();
const bookController = new BookController();


router.get('/books/:userId', authMiddleware, bookController.listBooks);
router.post('/books', authMiddleware, bookController.addBook);
router.get('/books', authMiddleware, bookController.listUsersBooks);

export default router;