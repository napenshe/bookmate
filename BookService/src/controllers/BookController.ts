import { Request, Response } from 'express';
import { BookService } from '../services/BookService';

export class BookController {
    private bookService: BookService;

    constructor() {
        this.bookService = new BookService();
    }

    public addBook = async (req: Request, res: Response): Promise<void> => {
        try {
            const bookInfo = {
                userId: req.user.id,
                title: req.body.title,
                author: req.body.author,
                isbn: req.body.isbn || null
            }
            const book = await this.bookService.addBook(bookInfo);
            res.status(201).json(book);
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };

    public listBooks = async (req: Request, res: Response): Promise<void> => {
        try {
            const books = await this.bookService.listBooks(parseInt(req.params.userId));
            res.status(200).json(books);
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };

    public listUsersBooks = async (req: Request, res: Response): Promise<void> => {
        try {
            const books = await this.bookService.listBooks(parseInt(req.user.id));
            res.status(200).json(books);
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };
}