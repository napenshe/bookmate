"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const BookController_1 = require("./src/controllers/BookController");
const AuthMiddleware_1 = require("./src/middleware/AuthMiddleware");
const config_1 = __importDefault(require("./src/config/config"));
const app = (0, express_1.default)();
const bookController = new BookController_1.BookController();
app.use(express_1.default.json());
app.get('/books/:userId', AuthMiddleware_1.authMiddleware, bookController.listBooks);
app.post('/books', AuthMiddleware_1.authMiddleware, bookController.addBook);
config_1.default.sync().then(() => {
    app.listen(process.env.PORT, () => {
        console.log('BookService is running');
    });
}).catch((err) => {
    console.error('Unable to connect to the database:', err);
});
