import express from 'express';
import sequelize from './src/config/config';
import bookRoutes from './src/routes/BookRoutes';

import { BookRepository } from './src/repositories/BookRepository'
import { connectRabbitMQ } from './src/utils/RabbitMQ';

const app = express();

app.use(express.json());
app.use(bookRoutes);

sequelize.sync().then(() => {
  app.listen(process.env.PORT, () => {
    console.log('BookService is running');
    setupRabbitMQListener();
  });
}).catch((err) => {
  console.error('Unable to connect to the database:', err);
});

async function setupRabbitMQListener() {
  try {
    const channel = await connectRabbitMQ();
    await channel.assertQueue('bookAvailability', { durable: true });

    console.log("Waiting for messages in 'bookAvailability' queue...");

    channel.consume('bookAvailability', async (msg) => {
      if (msg) {
        const { bookId, available } = JSON.parse(msg.content.toString());
        const bookRepository = new BookRepository();
        await bookRepository.updateBookAvailability(bookId, available);
        console.log(`Updated availability of book ${bookId} to ${available}`);
        channel.ack(msg);
      }
    }, { noAck: false });
  } catch (error) {
    console.error('Failed to setup RabbitMQ listener:', error);
  }
}