import express from 'express';
import sequelize from './src/config/config';
import { ProfileRepository } from './src/repositories/ProfileRepository';
import profileRoutes from './src/routes/ProfileRoutes';
import { connectRabbitMQ } from './src/utils/RabbitMQ';

const app = express();

app.use(express.json());
app.use(profileRoutes);

sequelize.sync().then(() => {
    app.listen(process.env.PORT, () => {
        console.log('ProfileService is running');
        setupRabbitMQListener();
    });
}).catch((err) => {
    console.error('Unable to connect to the database:', err);
});

async function setupRabbitMQListener() {
    try {
      const channel = await connectRabbitMQ();
      await channel.assertQueue('userRegistry', { durable: true });
  
      console.log("Waiting for messages in 'userRegistry' queue...");
  
      channel.consume('userRegistry', async (msg) => {
        if (msg) {
          const { userId } = JSON.parse(msg.content.toString());
          const bookRepository = new ProfileRepository();
          await bookRepository.createProfile({"userId": userId});
          console.log(`Created profile for user with id ${userId}}`);
          channel.ack(msg);
        }
      }, { noAck: false });
    } catch (error) {
      console.error('Failed to setup RabbitMQ listener:', error);
    }
  }