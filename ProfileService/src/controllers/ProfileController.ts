import { Request, Response } from 'express';
import { ProfileService } from '../services/ProfileService';

export class ProfileController {
    private profileService: ProfileService;

    constructor() {
        this.profileService = new ProfileService();
    }

    public createProfile = async (req: Request, res: Response): Promise<void> => {
        try {
            const profile = await this.profileService.createProfile(req.body);
            res.status(201).json(profile);
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };

    public getProfile = async (req: Request, res: Response): Promise<void> => {
        try {
            const profile = await this.profileService.getProfile(parseInt(req.user.id));
            if (profile) {
                res.status(200).json(profile);
            } else {
                res.status(404).json({ message: 'Profile not found' });
            }
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };

    public updateProfile = async (req: Request, res: Response): Promise<void> => {
        try {
            const profile = await this.profileService.updateProfile(parseInt(req.user.id), req.body);
            res.status(200).json(profile);
        } catch (error) {
            res.status(500).json({ error: error });
        }
    };
}