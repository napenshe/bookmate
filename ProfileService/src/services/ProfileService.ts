import { ProfileRepository } from '../repositories/ProfileRepository';
import { Profile } from '../models/Profile';

export class ProfileService {
    private profileRepository: ProfileRepository;

    constructor() {
        this.profileRepository = new ProfileRepository();
    }

    public async createProfile(details: any): Promise<Profile> {
        return this.profileRepository.createProfile(details);
    }

    public async getProfile(userId: number): Promise<Profile | null> {
        return this.profileRepository.findProfileByUserId(userId);
    }

    public async updateProfile(userId: number, details: any): Promise<Profile | null> {
        await this.profileRepository.updateProfile(userId, details);
        return this.getProfile(userId);
    }
}