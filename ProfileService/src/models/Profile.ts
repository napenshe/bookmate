import { DataTypes, Model } from 'sequelize';
import sequelize from '../config/config';

export class Profile extends Model {
    public userId!: number;
    public bio?: string;
    public location?: string;
}

Profile.init({
  userId: {
    type: DataTypes.INTEGER,
    primaryKey: true
  },
  bio: {
    type: DataTypes.TEXT,
    allowNull: true,
    defaultValue: null
  },
  location: {
    type: DataTypes.STRING,
    allowNull: true,
    defaultValue: null
  }
}, {
  sequelize,
  modelName: 'Profile'
});