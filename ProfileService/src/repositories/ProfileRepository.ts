import { Profile } from '../models/Profile';

export class ProfileRepository {
    public async createProfile(profileDetails: any): Promise<Profile> {
        return Profile.create(profileDetails);
    }

    public async findProfileByUserId(userId: number): Promise<Profile | null> {
        return Profile.findOne({ where: { userId } });
    }

    public async updateProfile(userId: number, profileDetails: any): Promise<[number, Profile[]]> {
        return Profile.update(profileDetails, { where: { userId }, returning: true});
    }
}