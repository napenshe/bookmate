import { Sequelize } from 'sequelize';
import 'dotenv/config';

const sequelize = new Sequelize({
    database: process.env.POSTGRES_DB,
    dialect: 'postgres',
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    host: process.env.DB_HOST,
  });
  

export default sequelize;