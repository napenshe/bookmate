import { Router } from 'express';
import { ProfileController } from '../controllers/ProfileController';
import { authMiddleware } from '../middleware/AuthMiddleware';

const router = Router();
const profileController = new ProfileController();

router.post('/profiles', authMiddleware, profileController.createProfile);
router.get('/profiles/me', authMiddleware, profileController.getProfile);
router.put('/profiles/me', authMiddleware, profileController.updateProfile);

export default router;